package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/cwink/go-game/game"
)

// Text represents a text/font object.
type Text struct {
	font     *ttf.Font
	position *game.Position
	color    *game.Color
}

// NewText creates a new Text object.
func NewText(file string, size uint, color *game.Color) (text *Text, err error) {
	text = new(Text)
	text.position = game.NewPosition(0.0, 0.0)
	text.color = game.NewColor(color.Red, color.Green, color.Blue, color.Alpha)
	text.font, err = ttf.OpenFont("assets/opensans.ttf", int(size))

	if err != nil {
		return nil, err
	}

	return text, nil
}

// Close destroys the text object.
func (text *Text) Close() {
	text.font.Close()
}

// Draw renders the given message to the screen.
func (text *Text) Draw(renderer *sdl.Renderer, message string, centered bool) error {
	surface, err := text.font.RenderUTF8Solid(message, sdl.Color{R: text.color.Red, G: text.color.Green, B: text.color.Blue, A: text.color.Alpha})

	if err != nil {
		return err
	}

	defer surface.Free()

	texture, err := renderer.CreateTextureFromSurface(surface)

	if err != nil {
		return nil
	}

	defer texture.Destroy()

	source := &sdl.Rect{X: 0, Y: 0, W: surface.W, H: surface.H}

	destination := &sdl.Rect{X: int32(text.position.X()), Y: int32(text.position.Y()), W: surface.W, H: surface.H}
	if centered {
		destination = &sdl.Rect{X: int32(windowWidth/2.0 - float64(surface.W)/2.0), Y: int32(windowHeight/2.0 - float64(surface.H)/2.0), W: surface.W, H: surface.H}
	}

	if err := renderer.Copy(texture, source, destination); err != nil {
		return nil
	}

	return nil
}
