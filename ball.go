package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/cwink/go-game/game"
)

// Ball represents a ball game object.
type Ball struct {
	position *game.Position
	velocity *game.Velocity
	color    *game.Color
	radius   float64
}

// Position retrieves the position object in ball.
func (ball *Ball) Position() *game.Position {
	return ball.position
}

// Velocity retrieves the velocity object in ball.
func (ball *Ball) Velocity() *game.Velocity {
	return ball.velocity
}

// Color retrieves the color object in ball.
func (ball *Ball) Color() *game.Color {
	return ball.color
}

// SetPosition sets the position in the ball.
func (ball *Ball) SetPosition(position *game.Position) {
	ball.position.Copy(position)
}

// SetVelocity sets the velocity in the ball.
func (ball *Ball) SetVelocity(velocity *game.Velocity) {
	ball.velocity.CopyVelocity(velocity)
}

// SetColor sets the color in the ball.
func (ball *Ball) SetColor(color *game.Color) {
	ball.color.Copy(color)
}

// Radius retrieves the radius of the ball.
func (ball *Ball) Radius() float64 {
	return ball.radius
}

// SetRadius sets the radius of the ball.
func (ball *Ball) SetRadius(value float64) {
	ball.radius = value
}

// Move takes the ball and applies its velocity.
func (ball *Ball) Move(deltaTime float64) {
	ball.position.SetX(ball.Position().X() + ball.velocity.X()*ball.velocity.Magnitude()*deltaTime)
	ball.position.SetY(ball.Position().Y() + ball.velocity.Y()*ball.velocity.Magnitude()*deltaTime)
}

// NewBall creates a new Ball object.
func NewBall(radius float64, color *game.Color) (ball *Ball) {
	ball = new(Ball)
	ball.position = game.NewPosition(0.0, 0.0)
	ball.velocity = game.NewVelocity(0.0, 0.0, 1.0)
	ball.color = game.NewColor(color.Red, color.Green, color.Blue, color.Alpha)
	ball.radius = radius
	return ball
}

// Draw renders the ball to the screen.
func (ball *Ball) Draw(renderer *sdl.Renderer) error {
	if err := renderer.SetDrawColor(ball.color.Red, ball.color.Green, ball.color.Blue, ball.color.Alpha); err != nil {
		return err
	}

	for width := 0.0; width < ball.radius*2.0; width++ {
		for height := 0.0; height < ball.radius*2.0; height++ {
			deltaX := ball.radius - width
			deltaY := ball.radius - height
			if deltaX*deltaX+deltaY*deltaY <= ball.radius*ball.radius {
				if err := renderer.DrawPoint(int32(ball.position.X()+deltaX), int32(ball.position.Y()+deltaY)); err != nil {
					return err
				}
			}
		}
	}

	return nil
}
