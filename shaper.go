package main

import (
	"gitlab.com/cwink/go-game/game"
)

// Shaper is an interface for a Shape type object.
type Shaper interface {
	Position() *game.Position
	Velocity() *game.Velocity
	Color() *game.Color
	SetPosition(position *game.Position)
	SetVelocity(velocity *game.Velocity)
	SetColor(color *game.Color)
	Move(deltaTime float64)
}
