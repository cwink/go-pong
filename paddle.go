package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/cwink/go-game/game"
)

// Paddle represents a paddle game object.
type Paddle struct {
	position *game.Position
	velocity *game.Velocity
	color    *game.Color
	radiusX  float64
	radiusY  float64
}

// Position retrieves the position object in paddle.
func (paddle *Paddle) Position() *game.Position {
	return paddle.position
}

// Velocity retrieves the velocity object in paddle.
func (paddle *Paddle) Velocity() *game.Velocity {
	return paddle.velocity
}

// Color retrieves the color object in paddle.
func (paddle *Paddle) Color() *game.Color {
	return paddle.color
}

// SetPosition sets the position in the paddle.
func (paddle *Paddle) SetPosition(position *game.Position) {
	paddle.position.Copy(position)
}

// SetVelocity sets the velocity in the paddle.
func (paddle *Paddle) SetVelocity(velocity *game.Velocity) {
	paddle.velocity.CopyVelocity(velocity)
}

// SetColor sets the color in the paddle.
func (paddle *Paddle) SetColor(color *game.Color) {
	paddle.color.Copy(color)
}

// RadiusX retrieves the X radius of the paddle.
func (paddle *Paddle) RadiusX() float64 {
	return paddle.radiusX
}

// RadiusY retrieves the Y radius of the paddle.
func (paddle *Paddle) RadiusY() float64 {
	return paddle.radiusY
}

// SetRadiusX sets the X radius of the paddle.
func (paddle *Paddle) SetRadiusX(value float64) {
	paddle.radiusX = value
}

// SetRadiusY sets the Y radius of the paddle.
func (paddle *Paddle) SetRadiusY(value float64) {
	paddle.radiusY = value
}

// Move takes the paddle and applies its velocity.
func (paddle *Paddle) Move(deltaTime float64) {
	paddle.position.SetX(paddle.Position().X() + paddle.velocity.X()*paddle.velocity.Magnitude()*deltaTime)
	paddle.position.SetY(paddle.Position().Y() + paddle.velocity.Y()*paddle.velocity.Magnitude()*deltaTime)
}

// NewPaddle creates a new Paddle object.
func NewPaddle(width float64, height float64, color *game.Color) (paddle *Paddle) {
	paddle = new(Paddle)
	paddle.color = game.NewColor(color.Red, color.Green, color.Blue, color.Alpha)
	paddle.position = game.NewPosition(0.0, 0.0)
	paddle.velocity = game.NewVelocity(0.0, 0.0, 1.0)
	paddle.radiusX = width / 2.0
	paddle.radiusY = height / 2.0
	return paddle
}

// Draw renders the paddle to the screen.
func (paddle *Paddle) Draw(renderer *sdl.Renderer) error {
	if err := renderer.SetDrawColor(paddle.color.Red, paddle.color.Green, paddle.color.Blue, paddle.color.Alpha); err != nil {
		return err
	}

	rect := &sdl.Rect{X: int32(paddle.position.X() - paddle.RadiusX()), Y: int32(paddle.position.Y() - paddle.RadiusY()), W: int32(paddle.RadiusX() * 2.0), H: int32(paddle.RadiusY() * 2.0)}

	if err := renderer.FillRect(rect); err != nil {
		return err
	}

	return nil
}
