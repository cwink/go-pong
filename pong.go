package main

import (
	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
	"gitlab.com/cwink/go-game/game"
	"math"
	"strconv"
)

const (
	windowWidth  = 1024
	windowHeight = 768
)

func main() {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}

	defer sdl.Quit()

	if err := ttf.Init(); err != nil {
		panic(err)
	}

	defer ttf.Quit()

	if err := ttf.Init(); err != nil {
		panic(err)
	}

	window, err := sdl.CreateWindow("PONG!", sdl.WINDOWPOS_CENTERED, sdl.WINDOWPOS_CENTERED, int32(windowWidth), int32(windowHeight), sdl.WINDOW_SHOWN)

	if err != nil {
		panic(err)
	}

	defer window.Destroy()

	renderer, err := sdl.CreateRenderer(window, -1, 0)

	if err != nil {
		panic(err)
	}

	defer renderer.Destroy()

	paddle := NewPaddle(150.0, 20.0, &game.Color{Red: 255, Green: 0, Blue: 0, Alpha: 255})

	paddle.Position().SetX(float64(windowWidth) / 2.0)
	paddle.Position().SetY(float64(windowHeight) - paddle.RadiusY())

	ball := NewBall(15.0, &game.Color{Red: 0, Green: 255, Blue: 0, Alpha: 255})

	ball.Position().SetX(float64(windowWidth) / 2.0)
	ball.Position().SetY(float64(windowHeight) / 2.0)

	text, err := NewText("assets/opensans.ttf", 24, &game.Color{Red: 255, Green: 255, Blue: 255, Alpha: 255})

	if err != nil {
		panic(err)
	}

	defer text.Close()

	title, err := NewText("assets/opensans.ttf", 72, &game.Color{Red: 255, Green: 255, Blue: 255, Alpha: 255})

	if err != nil {
		panic(err)
	}

	defer title.Close()

	timeStep := game.NewTimestep(0.01)

	score := 0

	gameover := true

	running := true

	for running {
		timeStep.Update()

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				running = false
			}
		}

		if sdl.GetKeyboardState()[sdl.SCANCODE_ESCAPE] == 1 {
			running = false
		}

		if gameover && sdl.GetKeyboardState()[sdl.SCANCODE_SPACE] == 1 {
			paddle.Position().SetX(float64(windowWidth) / 2.0)
			paddle.Position().SetY(float64(windowHeight) - paddle.RadiusY())
			ball.Position().SetX(float64(windowWidth) / 2.0)
			ball.Position().SetY(float64(windowHeight) / 2.0)
			ball.Velocity().SetX(1.0)
			ball.Velocity().SetY(-1.0)
			score = 0
			gameover = false
			ball.Velocity().SetMagnitude(0.5)
		}

		if !gameover {
			paddle.Velocity().SetX(0.0)
			paddle.Velocity().SetY(0.0)
			paddle.Velocity().SetMagnitude(1.0)

			switch {
			case paddle.Position().X()+paddle.RadiusX() < float64(windowWidth) && sdl.GetKeyboardState()[sdl.SCANCODE_D] == 1:
				paddle.Velocity().SetX(1.0)
			case paddle.Position().X()-paddle.RadiusX() > 0.0 && sdl.GetKeyboardState()[sdl.SCANCODE_A] == 1:
				paddle.Velocity().SetX(-1.0)
			}

			switch {
			case ball.Position().X()+ball.Radius() >= float64(windowWidth) && ball.Velocity().X() > 0.0:
				ball.Velocity().SetX(ball.Velocity().X() * -1.0)
			case ball.Position().X()-ball.Radius() <= 0.0 && ball.Velocity().X() <= 0.0:
				ball.Velocity().SetX(ball.Velocity().X() * -1.0)
			case ball.Position().Y()+ball.Radius() >= float64(windowHeight) && ball.Velocity().Y() > 0.0:
				gameover = true
			case ball.Position().Y()-ball.Radius() <= 0.0 && ball.Velocity().Y() <= 0.0:
				ball.Velocity().SetY(ball.Velocity().Y() * -1.0)
			case ball.Position().Y()+ball.Radius() >= paddle.Position().Y()-paddle.RadiusY() && ball.Position().X() > paddle.Position().X()-paddle.RadiusX() && ball.Position().X() < paddle.Position().X()+paddle.RadiusX() && ball.Velocity().Y() > 0.0:
				if ball.Velocity().Y() > 0.0 {
					score++
				}
				x := ball.Position().X() - paddle.Position().X()
				y := ball.Position().Y() - paddle.Position().Y()
				magnitude := math.Sqrt(x*x + y*y)
				ball.Velocity().SetX(x / magnitude)
				ball.Velocity().SetY(y / magnitude)
				ball.Velocity().SetMagnitude(ball.Velocity().Magnitude() + 0.1)
			}
		}

		timeStep.Accumulate(func(deltaTime float64) {
			if !gameover {
				paddle.Move(deltaTime)
				ball.Move(deltaTime)
			}
		})

		if err := renderer.SetDrawColor(0, 0, 0, 255); err != nil {
			panic(err)
		}

		if err := renderer.Clear(); err != nil {
			panic(err)
		}

		if err := ball.Draw(renderer); err != nil {
			panic(err)
		}

		if err := paddle.Draw(renderer); err != nil {
			panic(err)
		}

		if err := text.Draw(renderer, "Score: "+strconv.Itoa(score), false); err != nil {
			panic(err)
		}

		if gameover {
			if err := title.Draw(renderer, "Press spacebar to continue.", true); err != nil {
				panic(err)
			}
		}

		renderer.Present()
	}
}
